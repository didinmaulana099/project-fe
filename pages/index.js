import axios from "axios";
import { useState, useEffect } from "react";
import Pagination from "../components/pagination";
import { paginate } from "../utils/paginate";

const Home = () => {
  const [posts, setPosts] = useState([]);
  const pageSize = 1;
  const [currentPage, setCurrentPage] = useState(1);
  useEffect(() => {
    const getPosts = async () => {
      const { data: res } = await axios.get(
        //route api
        "http://192.168.167.39:8000/api/article"
      );
      setPosts(res.data);
    };
    getPosts();
  }, []);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  
  const paginatePosts = paginate(posts, currentPage, pageSize);

  return (
    <div className="container">
      <head>
        <title>Articles Go Kampus</title>
      </head>
        <div>
          <br/>
          <h2>Article GO Kampus</h2>
          <br/>
          {paginatePosts.map((e) => (
            <div style={{ maxWidth:"400px", padding:"10"}}>
            <img
                src={e.path}
                alt='test'
                style={{width: "60%"}}
                />
                <h2>{e.title}</h2>
                <p>{e.content}</p>
                <div>{e.creator}</div>
                <br/>
              </div>
          ))}
        </div>
      <Pagination
        items={posts.length}
        pageSize={pageSize}
        currentPage={currentPage}
        onPageChange={handlePageChange}
      />
    </div>
  );
};

export default Home;